﻿const item = document.getElementById("item");
const templateCard = document.getElementById("template-card").content;
const fragment = document.createDocumentFragment();

const personajesSeleccionados = {}; // Objeto para almacenar los personajes seleccionados y sus datos

document.addEventListener("DOMContentLoaded", () => {
    fetchData();
});

const fetchData = async () => {
    try {
        const res = await fetch("/JSON/kimetsu.json");
        const data = await res.json();

        // Combina las "lunas_superiores", "lunas_menguantes" y "personajes" en una sola colección
        const lunasSuperiores = data.kimetsu_no_yaiba.lunas_superiores;
        const lunasMenguantes = data.kimetsu_no_yaiba.lunas_menguantes;
        const personajes = data.kimetsu_no_yaiba.personajes;
        const combinedData = lunasSuperiores.concat(lunasMenguantes, personajes);

        PintarCartas(combinedData);
    } catch (error) {
        console.log(error);
    }
};

const PintarCartas = data => {
    data.forEach(itemData => {
        const card = templateCard.cloneNode(true);

        // Rellenar los datos en la tarjeta
        card.querySelector('h5').textContent = itemData.nombre;
        card.querySelector('p').textContent = itemData.habilidades;
        card.querySelector('h1').textContent = `Poder: ${itemData.atributo_poder}`;

        // Configurar la imagen del personaje
        const imagen = card.querySelector('img');
        imagen.src = itemData.imagen; // Establece la fuente de la imagen con la URL del JSON
        imagen.alt = `Imagen de ${itemData.nombre}`; // Agrega un atributo "alt" para accesibilidad
        imagen.style.width = "280px"; // Establece el ancho deseado (ajusta según tus preferencias)
        imagen.style.height = "150px";

        // Botón para seleccionar el personaje
        const botonSeleccionar = card.querySelector('.btn-primary');
        botonSeleccionar.textContent = 'Seleccionar Personaje';

        // Contador de clics para el personaje
        const contadorClics = document.createElement('span');
        contadorClics.textContent = 'Clics: 0';
        card.querySelector('.card-body').appendChild(contadorClics);

        // Manejador de clics para el botón "Seleccionar Personaje"
        botonSeleccionar.addEventListener('click', () => {
            event.preventDefault();
            const nombrePersonaje = itemData.nombre;

            if (!personajesSeleccionados[nombrePersonaje]) {
                personajesSeleccionados[nombrePersonaje] = {
                    habilidades: itemData.habilidades,
                    poder: itemData.atributo_poder,
                    clics: 0
                };
            }

            personajesSeleccionados[nombrePersonaje].clics++;
            const poderTotal = personajesSeleccionados[nombrePersonaje].poder * personajesSeleccionados[nombrePersonaje].clics;

            contadorClics.textContent = `Matar: ${personajesSeleccionados[nombrePersonaje].clics}, Poder Total: ${poderTotal}`;

            // Actualizar la tabla de personajes seleccionados
            actualizarTablaPersonajesSeleccionados(personajesSeleccionados);
        });

        // Agregar la tarjeta clonada al fragmento
        fragment.appendChild(card);
    });

    // Agregar el fragmento con las tarjetas al elemento "item"
    item.appendChild(fragment);
};

function actualizarTablaPersonajesSeleccionados(personajesSeleccionados) {
    const tablaPersonajesSeleccionados = document.getElementById("tabla-personajes-seleccionados");
    tablaPersonajesSeleccionados.innerHTML = ""; // Limpiar la tabla antes de actualizar

    for (const nombre in personajesSeleccionados) {
        const personaje = personajesSeleccionados[nombre];
        const fila = document.createElement('tr');
        const poderTotal = personaje.poder * personaje.clics;
        fila.innerHTML = `<td>${nombre}</td><td>${personaje.habilidades.join(", ")}</td><td>Clics: ${personaje.clics}, Poder Total: ${poderTotal}</td>`;
        tablaPersonajesSeleccionados.appendChild(fila);
    }
}
